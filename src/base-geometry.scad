/******************************************************************************
                    Basic Geometries - Electronics Enclosure
*******************************************************************************

Here are geometric elements that are shared between all parts of the enclosure.

This is part of an electronics enclosure.
*/


use <helpers.scad>
use <boxes.scad>


$fa = 2;
$fs = 0.25;


/* The outer surface of the body shell */
module body_shell_outer (
    length=200,
    width=100,
    height=50,
    radius=10,
    shell_wall_thickness=5,
    $fa=$fa,
    $fs=$fs
) {
    translate([0, 0, height])
    rotate([0, 90, 0])
    roundedCube([height, width, length,], r=radius, sidesonly=true, center=false);
}
// body_shell_outer();


/* The minimum inner radius of the body shell. */
function body_shell_inner_radius_min() = 1.0;

/* The inner surface of the body shell */
module body_shell_inner (
    length=200,
    width=100,
    height=50,
    radius=10,
    shell_wall_thickness=5,
    $fa=$fa,
    $fs=$fs
) {
    inner_r = radius - shell_wall_thickness;
    edge_r = max(inner_r, body_shell_inner_radius_min());

    translate([
        0      - 0.01,
        0      + shell_wall_thickness,
        height - shell_wall_thickness
    ])
    rotate([0, 90, 0])
    roundedCube(
        [
            height - 2*shell_wall_thickness,
            width  - 2*shell_wall_thickness,
            length + 0.02,
        ],
        r=edge_r,
        sidesonly=true,
        center=false);
}
// body_shell_inner();


/* Compute the height of a rib, from the width of the enclosure.

Really you should use its longest side. The enclosure is assumed to be
relatively wide and not tall.
*/
function get_rib_height(shell_width) = 0.06 * shell_width;


/* A rib for reinforcement of the shell.

A reinforcing structure that runs transversely.
*/
module rib(
    // Dimensions of the rib itself
    width=3,
    height=5,
    // Dimensions of the body shell where the rib is located
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    ds = 0.02;

    translate([
        0,
        shell_wall_thickness - ds,
        shell_wall_thickness - ds
    ])
    difference() {
        // Outer circumference of the rib
        body_shell_outer(
            length=width,
            width= shell_width  - 2*shell_wall_thickness + 2*ds,
            height=shell_height - 2*shell_wall_thickness + 2*ds,
            radius=shell_radius - shell_wall_thickness + ds,
            shell_wall_thickness=height + ds,
            $fa=$fa,
            $fs=$fs
        );
        // Inner hole of the rib
        body_shell_inner(
            length=width,
            width= shell_width  - 2*shell_wall_thickness + 2*ds,
            height=shell_height - 2*shell_wall_thickness + 2*ds,
            radius=shell_radius - shell_wall_thickness + ds,
            shell_wall_thickness=height + ds,
            $fa=$fa,
            $fs=$fs
        );
    };
}
// rib();


/* A horizontal opening for a vent */
module opening_horizontal(
    opening_width=3,
    // Possible values: "bottom", "top"
    position = "bottom",
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    dz = position == "top"
       ? shell_height - shell_wall_thickness
       : 0;

    translate([0, 0, dz])
    translate([opening_width, shell_radius, -0.01])
    rotate([0, 0, 90])
    rounded_bar(
        shell_width - 2*shell_radius,
        opening_width,
        shell_wall_thickness + 0.02
        );
}
// opening_horizontal();


/* A vertical opening for a vent */
module opening_vertical(
    opening_width=3,
    // Possible values: "front", "rear"
    position = "front",
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    dy = position == "rear"
       ? shell_width - shell_wall_thickness
       : 0;

    translate([0, dy, 0])
    translate([opening_width, shell_wall_thickness + 0.01, shell_radius])
    rotate([0, -90, 90])
    rounded_bar(
        shell_height - 2*shell_radius,
        opening_width,
        shell_wall_thickness + 0.02
        );
}
// opening_vertical();


/* Four vent openings on all four sides of the shell. */
module opening_quad(
    opening_width=3,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    opening_horizontal(
        opening_width=opening_width, position = "bottom",
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
    opening_horizontal(
        opening_width=opening_width, position = "top",
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
    opening_vertical(
        opening_width=opening_width, position = "front",
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
    opening_vertical(
        opening_width=opening_width, position = "rear",
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
}
// opening_quad();


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer();
        body_shell_inner();
    }

    // One rib
    translate([190 - 3, 0, 0])
    rib();

    // // One stringer
    // stringer();

    // One set of vent openings on each side of the shell
    opening_quad();
}
test_geometry();
