//##############################################################################
//                               Helpers
//##############################################################################
//
// Helper functions and modules


// Basic geometries
// -----------------------------------------------------------------------------

// 2D
// ---------------------------------------
/* Create a circular wedge, or pizza slice, with the tip at the origin. */
module circle_sector(r=10, angle=30, $fa=$fa){
    n_points = ceil(angle / $fa);
    angle_1 = angle / n_points;

    angles = [for (i = [0:n_points]) i * angle_1];
    coords = [
        [0.0, 0.0],
        for (th=angles) [r*cos(th), r*sin(th)]
    ];
    polygon(coords);
 }
//  circle_sector();


/* 2D polygon that consists of a row of triangles. */
module teeth_2d(
    teeth_size=2,
    teeth_length=160
) {
    n_teeth = floor(teeth_length / teeth_size);
    teeth_length_real = teeth_size * n_teeth;
    teeth_thick = teeth_size / 2;

    polygon([
        [teeth_length_real, 0],
        [teeth_length_real, -0.1],
        [0,                 -0.1],
        [0,                 0],
        for (i = [0:n_teeth - 1]) each [
            [(i + 0.5) * teeth_size, teeth_thick],
            [(i + 1)   * teeth_size, 0],
        ]
    ]);
}
// teeth_2d();


// 3D
// ---------------------------------------
module rounded_bar(length=10, width=2, height=3) {
    translate([width/2, width/2, 0])
    hull() {
        cylinder(d=width, h=height);
        translate([length - width, 0, 0])
        cylinder(d=width, h=height);
    }
}
// rounded_bar();


// Transformations
// -----------------------------------------------------------------------------

/*
A mirror module that retains the original object in addition to the mirrored
one.

Taken from:
    https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Tips_and_Tricks#Create_a_mirrored_object_while_retaining_the_original

Parameters
----------

v: vector[3]
    The mirror vector.
*/
module mirror_copy(v=[1, 0, 0]) {
    children();

    mirror(v)
    children();
}

/*
A rotation module that copies the original object and applies a cumulative
rotation to each copy.

Parameters
----------

r: vector[3]
    The rotation vector.

n: integer
    The number of result objects.
    The first object is not rotated.
*/
module rotate_copy(r=[10, 10, 10], n=2) {
    if (n <= 1) {
        children();
    } else {
        children();

        rotate(r)
        rotate_copy(r, n - 1) {
            children();
        }
    }
}

/*
A translation module that copies the original object and applies a cumulative
translation to each copy.

Parameters
----------

t: vector[3]
    The translation vector.

n: integer
    The number of result objects.
    The first object is not rotated.
*/
module translate_copy(t=[10, 10, 10], n=2) {
    if (n <= 1) {
        children();
    } else {
        children();

        translate(t)
        translate_copy(t, n - 1) {
            children();
        }
    }
}
