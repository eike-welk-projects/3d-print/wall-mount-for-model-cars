/******************************************************************************
                    Wall Mount - Wall Mount for Model Cars
*******************************************************************************

This is the wall mount itself.

This is part of a wall mount for model cars.
*/


use <helpers.scad>
use <boxes.scad>


$fa = 2;
$fs = 0.25;


/* A ring, similar parameters like `cylinder` */
module ring(
    height=10,
    diameter=20,
    material_thickness=2,
    center=false
) {
    ds = 0.02;
    tz = center ? 0 : -ds / 2;

    difference() {
        cylinder(h=height, d=diameter, center=center);
        translate([0, 0, tz])
        cylinder(
            h=height + ds,
            d=diameter - 2*material_thickness,
            center=center
        );
    }
}
// ring();
// ring(center=true);

/* A ring with inward facing rims at both ends.

The rims have the same material thickness as the rest of the ring.
*/
module ring_rimmed(
    height=10,
    diameter=20,
    rim_height=1,
    material_thickness=2,
    center=false
) {
    tz = center ? -height / 2 : 0;

    translate([0, 0, tz]) {
        // Ring
        ring(height, diameter, material_thickness, false);
        // Rims
        translate_copy([0, 0, height - material_thickness])
        ring(
            height=material_thickness,
            diameter=diameter,
            material_thickness=material_thickness + rim_height,
            center=false
        );
    }
}
// ring_rimmed();
// ring_rimmed(center=true);

/* Round geometries that hold the wheels

A `ring_rimmed` with most of it cut off.
*/
module wheel_cup(
    cup_width = 13,
    cup_diameter=32,
    material_thickness=2
) {
    ds = 0.02;

    translate([0, -cup_diameter/2, 0])
    difference() {
        // The cup itself
        rotate([0, 90, 0])
        ring_rimmed(
            height=cup_width,
            diameter=cup_diameter,
            rim_height=material_thickness,
            material_thickness=material_thickness,
            center=true
        );

        // Cut off the top half
        translate([0, 0, cup_diameter/2])
        cube([cup_width + ds, cup_diameter + ds, cup_diameter], center=true);
        // Cut off som more at the front
        translate([0, -cup_diameter/2, cup_diameter * 0.25])
        cube([cup_width + ds, cup_diameter, cup_diameter], center=true);
    }
}
// wheel_cup();

/* The plate that is screwed to the wall */
module base_plate(
    plate_width = 13,
    cup_diameter=32,
    corner_r=2,
    material_thickness=2
) {
    plate_height_base = 0.5 * cup_diameter;
    plate_height_top = 0.2 * cup_diameter;
    plate_height_bot = 0.5 * cup_diameter;
    plate_height = plate_height_base + plate_height_top + plate_height_bot;

    translate([0, -material_thickness/2, -plate_height/2 + plate_height_top])
    rotate([90, 0, 0])
    roundedCube(
        [plate_width, plate_height, material_thickness],
        r=corner_r,
        sidesonly=true,
        center=true
    );
}
// base_plate();

/* One curved triangular rib */
module rib_1(
    cup_diameter=32,
    material_thickness=2
) {
    ds = 0.02;
    rib_height = cup_diameter / 2;

    difference() {
        union() {
            // The small rounded triangle between cup and base plate
            translate([0, -rib_height/2, rib_height/2 - cup_diameter/2])
            cube([material_thickness, rib_height, rib_height], center=true);

            // The triangle below the cup
            translate(
                [-material_thickness/2, -rib_height, -cup_diameter/2 + ds]
            )
            rotate([0, 90 ,0])
            linear_extrude(height=material_thickness)
            polygon([
                [0,             0           ],
                [rib_height,    rib_height  ],
                [0,             rib_height  ],
            ]);
        }

        // The space for the wheel cup
        translate([0, -(cup_diameter - ds)/2, 0])
        rotate([0, 90, 0])
        cylinder(h=material_thickness + ds, d=cup_diameter - ds, center=true);
    }
}

/* Two ribs in a symmetric arrangement */
module ribs(
    screw_head_d = 5.5,
    cup_diameter=32,
    material_thickness=2
) {
    rib_dist = screw_head_d + material_thickness;

    translate([-rib_dist/2, 0, 0])
    translate_copy([rib_dist, 0, 0])
    rib_1(
        cup_diameter=cup_diameter,
        material_thickness=material_thickness
    );
}
// ribs();

// spell-checker: ignore sidesonly
/* Structure that holds one wheel of the model car

This structure holds one wheel of the model car to the wall.
It is screwed to the wall, All forces are primarily conducted inside this
structure.

All dimensions are the actual print dimension, no extra space for tolerances is
incorporated in this module.
 */
module wheel_holder(
    // Properties of the model car's wheel
    wheel_width=10,
    wheel_diameter=28,
    // Properties of the screw
    screw_d=3.0,
    screw_head_d=5.5,
    // Holder specific properties
    corner_r=2,
    material_thickness=2,
) {
    ds = 0.02;
    cup_width = wheel_width + 2 * material_thickness;
    cup_diameter = wheel_diameter + 2 * material_thickness;

    difference() {
        union() {
            // The base plate
            plate_width = max(
                cup_width,
                screw_head_d + 2*material_thickness + 2*corner_r
            );
            base_plate(
                plate_width=plate_width,
                cup_diameter=cup_diameter,
                corner_r=corner_r,
                material_thickness=material_thickness
            );

            // Ribs
            ribs(
                screw_head_d=screw_head_d,
                cup_diameter=cup_diameter,
                material_thickness=material_thickness
            );

            // The cup where the wheel rests
            wheel_cup(
                cup_width=cup_width,
                cup_diameter=cup_diameter,
                material_thickness=material_thickness
            );
        }

        // Screw hole
        // TODO: Make it a slot
        translate([0, 0, -cup_diameter/2 - screw_d])
        translate([0, -material_thickness/2, 0])
        rotate([90, 0, 0])
        roundedCube(
            [screw_d, screw_d * 3, material_thickness+ds],
            r=screw_d/2 - ds,
            sidesonly=true,
            center=true
        );
        // cylinder(h=material_thickness+ds, d=screw_d);
    }
}
// wheel_holder(screw_d=3);

/* A strip of material, between the wheel holders

It fixes the two wheel holders at the correct distance.
*/
module distance_strip(
    length=50,
    width=10,
    material_thickness=2
) {
    translate([0, -material_thickness, -width])
    cube([length, material_thickness, width]);
}
// distance_strip();

/* Part to hang a model car to a wall

All dimensions are true print dimensions, extra space for tolerances must be
added in higher level modules.
*/
module wall_mount(
    // Properties of the model car's wheel
    wheel_center_distance=74,
    wheel_width=10,
    wheel_diameter=28,
    // Properties of the screw
    screw_d=3.0,
    screw_head_d=5.5,
    // Holder specific properties
    corner_r=2,
    material_thickness=2,
) {
    // The two wheel holders
    translate_copy([wheel_center_distance, 0, 0])
    wheel_holder(
        // Properties of the model car's wheel
        wheel_width=wheel_width,
        wheel_diameter=wheel_diameter,
        // Properties of the screw
        screw_d=screw_d,
        screw_head_d=screw_head_d,
        // Holder specific properties
        corner_r=corner_r,
        material_thickness=material_thickness
    );

    // The strip between the holders
    distance_strip(
        length=wheel_center_distance,
        width=wheel_diameter * 0.35,
        material_thickness=material_thickness
    );
}
// wall_mount();

/* Pad(s) for the lower wheels

Flat pads that protect the wall against dirt.
*/
module wheel_pad(
    // Properties of the model car's wheel
    wheel_center_distance=74,
    wheel_width=10,
    wheel_diameter=28,
    // Properties of the screw
    screw_d=3.0,
    screw_head_d=5.5,
    // Holder specific properties
    corner_r=2,
    material_thickness=2,
) {
    cup_width = wheel_width + 2 * material_thickness;
    cup_diameter = wheel_diameter + 2 * material_thickness;

    // Two base plates
    translate_copy([wheel_center_distance, 0, 0]) {
        // The base plate
        plate_width = max(
            cup_width,
            screw_head_d + 2*material_thickness + 2*corner_r
        );
        base_plate(
            plate_width=plate_width,
            cup_diameter=cup_diameter,
            corner_r=corner_r,
            material_thickness=material_thickness
        );
    }

    // The strip between the base plates
    distance_strip(
        length=wheel_center_distance,
        width=wheel_diameter * 0.35,
        material_thickness=material_thickness
    );
}
// wheel_pad();

/* All parts of the wall mount */
module wall_mount_complete(
    // Properties of the model car's wheels
    wheel_outer_distance=74,
    wheel_width=7.6,
    wheel_diameter=26,
    // Properties of the screw
    screw_d=2.9,
    screw_head_d=5.5,
    // Holder specific properties
    corner_r=2,
    material_thickness=2
) {
    wheel_center_distance = wheel_outer_distance - wheel_width;
    wheel_width_ex = wheel_width * 1.2;
    wheel_diameter_ex = wheel_diameter * 1.07;

    screw_d_ex = screw_d * 1.05;
    screw_head_d_ex = screw_head_d * 1.2;

    wall_mount(
        // Properties of the model car's wheel
        wheel_center_distance=wheel_center_distance,
        wheel_width=wheel_width_ex,
        wheel_diameter=wheel_diameter_ex,
        // Properties of the screw
        screw_d=screw_d_ex,
        screw_head_d=screw_head_d_ex,
        // Holder specific properties
        corner_r=corner_r,
        material_thickness=material_thickness
    );

    translate([0, 0, -2 * wheel_diameter])
    wheel_pad(
        // Properties of the model car's wheel
        wheel_center_distance=wheel_center_distance,
        wheel_width=wheel_width_ex,
        wheel_diameter=wheel_diameter_ex,
        // Properties of the screw
        screw_d=screw_d_ex,
        screw_head_d=screw_head_d_ex,
        // Holder specific properties
        corner_r=corner_r,
        material_thickness=material_thickness
    );
}
wall_mount_complete();
