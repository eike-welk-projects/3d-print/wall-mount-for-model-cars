/******************************************************************************
                    Print Parts - Wall Mount for Model Cars
*******************************************************************************

The parts of the wall mount arranged for printing

This is part of a wall mount for model cars.
*/


use <helpers.scad>
use <wall-mount.scad>


$fa = 2;
$fs = 0.25;


// spell-checker: ignore Wltoys, Tamiya

/* Configuration for Wltoys K989 */
rotate([-90, 0, 0])
wall_mount_complete(
    // Properties of the model car's wheels
    wheel_outer_distance=74,
    wheel_width=8.0,
    wheel_diameter=26,
    // Properties of the screws, that are used to mount the holder to the wall
    // 1.
    // Wall plug diameter 4 mm: screw diameter 3 mm
    // Holder fits both axles.
    // The head of a diameter 3.5 screw can be forced into place.
    screw_d=3.0,
    screw_head_d=5.5,
    // // 2.
    // // Wall plug diameter 5 mm: screw diameter 3.5 mm
    // // Holder fits only to front wheels
    // screw_d=3.5,
    // screw_head_d=7.0,
    // // 3.
    // // Compromise: Wall plug diameter 5 mm: screw diameter 3.5 mm
    // // The holder fits barely to rear wheels, but well to front wheels.
    // screw_d=3.5,
    // screw_head_d=6.0,
    // Holder specific properties
    corner_r=2,
    material_thickness=2
);


// /* Configuration for Tamiya TT-02 */
// rotate([-90, 0, 0])
// wall_mount_complete(
//     // Properties of the model car's wheels
//     wheel_outer_distance=182,
//     wheel_width=27,
//     wheel_diameter=66,
//     // Properties of the screws, that are used to mount the holder to the wall
//     screw_d=4.5,
//     screw_head_d=8.8,
//     // Holder specific properties
//     corner_r=2,
//     material_thickness=2
// );


// /* Configuration for HSP-94186 */
// rotate([-90, 0, 0])
// wall_mount_complete(
//     // Properties of the model car's wheels
//     wheel_outer_distance=225,
//     wheel_width=40,
//     wheel_diameter=76,
//     // Properties of the screws, that are used to mount the holder to the wall
//     screw_d=4.5,
//     screw_head_d=8.8,
//     // Holder specific properties
//     corner_r=2,
//     material_thickness=2
// );
